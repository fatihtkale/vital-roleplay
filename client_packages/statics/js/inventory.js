﻿let messagesLoaded = false;
let selected = undefined;
let timeout = undefined;

$(document).ready(function() {
	i18next.use(window.i18nextXHRBackend).init({
		backend: {
			loadPath: '../i18n/en.json'
		}
	}, function(err, t) {
        jqueryI18next.init(i18next, $);
		$(document).localize();
		messagesLoaded = true;
	});
});

function populateInventory(inventoryJson, title) {
	if(messagesLoaded) {
		// Initialize the selection
		selected = undefined;
		
		// Get the items in the inventory
		let inventory = JSON.parse(inventoryJson);
		
		// Get the item containers
		let titleContainer = document.getElementById('identifier');
		let inventoryContainer = document.getElementById('personal-inventory');

		// Add the text to the header
		titleContainer.textContent = i18next.t(title);
		
		for(let i = 0; i < inventory.length; i++) {
			// Get each item
			let item = inventory[i];
			
			// Create the elements to show the items
			let itemContainer = document.createElement('div');
			let amountContainer = document.createElement('div');
			let itemImage = document.createElement('img');
			
			// Get the needed classes
            itemContainer.classList.add('inventory-item ' + item.hash);
            amountContainer.classList.add('inventory-amount');
            
			
			// Get the content of each item
			itemImage.src = '../img/inventory/' + item.hash + '.png';
			amountContainer.textContent = item.amount;
			
			itemContainer.onclick = (function() {
				// Check if a new item has been selected
				if(selected !== i) {
					// Get the previous selection
					if(selected !== undefined) {
						let index = getItemIndexInArray(inventory, selected);
						let previousSelected = document.getElementsByClassName('inventory-item')[index];
						previousSelected.classList.remove('active-item');
					}
					
					// Select the clicked element
					let currentSelected = document.getElementsByClassName('inventory-item')[i];
					currentSelected.classList.add('active-item');
					selected = item.id;
					
					// Show the options
					mp.trigger('getInventoryOptions', item.type, item.hash);
				}
			});
			
			// Create the item hierarchy	
			inventoryContainer.appendChild(itemContainer);
			itemContainer.appendChild(amountContainer);
			itemContainer.appendChild(itemImage);		
		}	
		clearTimeout(timeout);
	} else {
		// Wait for the messages to be loaded
		clearTimeout(timeout);
		timeout = setTimeout(function() { populateInventory(inventoryJson, title); }, 100);
	}
}

function showInventoryOptions(optionsArrayJson, dropable) {
	if(messagesLoaded) {
		// Get the footer options
		let root = document.getElementById('item-options');
		
		// Clear the children
		while(root.firstChild) {
			root.removeChild(root.firstChild);
		}

		// Get the options
		let optionsArray = JSON.parse(optionsArrayJson);

		// Add the options
		for(let i = 0; i < optionsArray.length; i++) {

			// Create the container
			let optionContainer = document.createElement('div');

			// Add the text to the option
			optionContainer.textContent = i18next.t(optionsArray[i]);

			// Add the click event
			optionContainer.onclick = (function() {
				// Execute the selected option
				mp.trigger('executeAction', selected, i18next.t(optionsArray[i]));
			});

			// Add the container to the option list
			root.appendChild(optionContainer);
		}
		
		if(dropable) {
			// Add drop option
			let optionContainer = document.createElement('div');

			// Add the text to the option
			optionContainer.textContent = i18next.t('general.drop');

			// Add the click event
			optionContainer.onclick = (function() {
				// Execute the selected option
				mp.trigger('executeAction', selected, i18next.t('general.drop'));
			});

			// Add the container to the option list
			root.appendChild(optionContainer);
		}
			
		clearTimeout(timeout);
	} else {
		// Wait for the messages to be loaded
		clearTimeout(timeout);
		timeout = setTimeout(function() { showInventoryOptions(optionsArrayJson, dropable); }, 100);
	}
}

function updateInventory(item) {
	// Get the item from the JSON
	let itemObject = JSON.parse(item);

	// Get the selected HTML element
	let inventoryContainer = document.getElementById('inventory');

	for(let i = 0; i < inventoryContainer.children.length; i++) {
		// Get the current child
		let itemContainer = inventoryContainer.children[i];

		// Check if the hash is the same
		let imageParts = itemContainer.lastChild.src.split('/');
		let hash = imageParts[imageParts.length - 1].split('.')[0];
		
		if(itemObject.hash !== hash) continue;

		if(itemObject.amount === 0) {
			// Unselect the previous selection
			selected = undefined;
	
			while (itemContainer.firstChild) {
				// Remove all the children
				itemContainer.removeChild(itemContainer.firstChild);
			}

			// Remove the current element and options
			itemContainer.remove();

			// Get the footer options
			let root = document.getElementById('item-options');
		
			// Clear the children
			while(root.firstChild) {
				root.removeChild(root.firstChild);
			}			
		} else {
			// Update the amount
			itemContainer.firstChild.textContent = itemObject.amount;
		}

		break;
	}
}

function getItemIndexInArray(inventory, itemId) {
	for(let i = 0; i < inventory.length; i++) {
		if(inventory[i].id === itemId) {
			return i;
		}
	}
	
	return -1;
}	




"use strict";

jQuery.fn.extend({
    addRemoveItems: function(targetCount) {
        return this.each(function() {
            var $children = $(this).children();
            var rowCountDifference = targetCount - $children.length;
            console.log('row count diff: ' + rowCountDifference);
           
            if(rowCountDifference > 0)
            {
                // Add items
                for(var i = 0; i < rowCountDifference; i++)
                {
                    console.log($rows.first());
                    $children.last().clone().appendTo(this);
                }
            }
            else if(rowCountDifference < 0)
            {
                // remove items
                $children.slice(rowCountDifference).remove();
            }
        });
    },

    parentToAnimate: function(newParent, duration) {
        duration = duration || 'slow';
        
        var $element = $(this);
        //console.log($element);
        if($element.length > 0)
        {
            
            newParent = $(newParent); // Allow passing in either a JQuery object or selector
            var oldOffset = $element.offset();
            $(this).appendTo(newParent);
            var newOffset = $element.offset();
            
            
            var temp = $element.clone().appendTo('body');
            
            temp.css({
                'position': 'absolute',
                'left': oldOffset.left,
                'top': oldOffset.top,
                'zIndex': 1000
            });
            
            $element.hide();
                
            temp.animate({
                'top': newOffset.top,
                'left': newOffset.left
            }, duration, function() {
                $element.show();
                temp.remove();
            });
            
            //console.log("parentTo Animate done");
        }
    }
});

$('#row-count').on('input propertychange change', function() {
    var targetRowCount = $(this).val();
    //console.log('target count: ' + targetRowCount);
    $('label[for="'+$(this).attr('id')+'"]').html(targetRowCount);
      
    $('#personal-inventory.inventory-table').addRemoveItems(targetRowCount);
    
    refreshSortableInventoryList();
}).trigger('change');

$('#column-count').on('input propertychange change', function() {
    var targetColumnCount = $(this).val();
    //console.log('target count: ' + targetColumnCount);
    $('label[for="'+$(this).attr('id')+'"]').html(targetColumnCount);
        
    $('#personal-inventory.inventory-table .inventory-row').addRemoveItems(targetColumnCount);
    
    refreshSortableInventoryList();
}).trigger('change');




// Sorting, dragging, dropping, etc

refreshSortableInventoryList();
function refreshSortableInventoryList()
{
    $('.inventory-cell').sortable({
        connectWith: '.inventory-cell',
        placeholder: 'inventory-item-sortable-placeholder',
        receive: function( event, ui ) {
            var attrWhitelist = $(this).closest('.inventory-table').attr('data-item-filter-whitelist');
            var attrBlackList = $(this).closest('.inventory-table').attr('data-item-filter-blacklist');
            var itemFilterWhitelistArray = attrWhitelist ? attrWhitelist.split(/\s+/) : [];
            var itemFilterBlacklistArray = attrBlackList ? attrBlackList.split(/\s+/) : [];
            //console.log(itemFilterWhitelistArray);
            //console.log(itemFilterBlacklistArray);  
            
            var attrTypeList = $(ui.item).attr('data-item-type');
            var itemTypeListArray = attrTypeList ? attrTypeList.split(/\s+/) : [];
            //console.log(itemTypeListArray);
            
            var canMoveIntoSlot = verifyWithWhiteBlackLists(itemTypeListArray, itemFilterWhitelistArray, itemFilterBlacklistArray)
            
            if(!canMoveIntoSlot)
            {
                console.log("Can't move to this slot");
                $(ui.sender).sortable('cancel');
                $(ui.item).parentToAnimate($(ui.sender), 200);
            }
            else                
            {
            
                // Swap places of items if dragging on top of another
                // Add the items in this list to the list the new item was from
                $(this).children().not(ui.item).parentToAnimate($(ui.sender), 200);
                
                // $(this) is the list the item is being moved into
                // $(ui.sender) is the list the item came from
                // Don't forget the move swap items as well
                
                // $(this).attr('data-slot-position-x');
                // $(this).attr('data-slot-position-y');
                // $(ui.sender).attr('data-slot-position-x');
                // $(ui.sender).attr('data-slot-position-y');
                //console.log("Moving to: (" + $(this).attr('data-slot-position-x') + ", " + $(this).attr('data-slot-position-y') + ") - From: (" + $(ui.sender).attr('data-slot-position-x') + ", " + $(ui.sender).attr('data-slot-position-y') + ")");
            }
        }
    }).each(function() {
        // Setup some nice attributes for everything
        // Makes it easier to update the backend
        $(this).attr('data-slot-position-x', $(this).prevAll('.inventory-cell').length);
        $(this).attr('data-slot-position-y', $(this).closest('.inventory-row').prevAll('.inventory-row').length);
    }).disableSelection();
}

function verifyWithWhiteBlackLists(itemList, whiteList, blackList)
{
    // itemList should contain tags
    // whiteList and blackList can contain tags and tag queries
    
    // If we have a matching tags to some tag query in the whiteList but not in the blackList, then return true
    // Else return false
    
    
    console.group("Lists");
    console.log(itemList);
    console.log(whiteList);
    console.log(blackList);
    console.groupEnd();

    // If white and black lists are empty, return true
    // Save the calculations, no filtering
    if(whiteList.length == 0 && blackList.length == 0)
        return true;
    

    
    // Check if the itemList has an item in the blackList
    var inBlackList = false;
    $.each(blackList, function(index, value) {
        var itemBlack = value;
        var itemBlackAndArray = itemBlack.split(/\+/);
        console.log(itemBlackAndArray);
        
        var andedResult = true;
        for(var i = 0; i < itemBlackAndArray.length; i++)
        {
            if(blackList.length > 0 && $.inArray(itemBlackAndArray[i], itemList) !== -1)
            {
                andedResult = andedResult && true;
            }
            else
            {
                andedResult = andedResult && false;
            }
        }
        
        if(andedResult)
            inBlackList = true;
    });
    
    inBlackList = blackList.length > 0 ? inBlackList : false;
    
    
    // Check if the itemList has an item in the whiteList
    var inWhiteList = false;
    $.each(whiteList, function(index, value) {
        var itemWhite = value;
        var itemWhiteAndArray = itemWhite.split(/\+/);
        //console.log(itemWhiteAndArray);
        
        var andedResult = true;
        for(var i = 0; i < itemWhiteAndArray.length; i++)
        {
            if(whiteList.length > 0 && $.inArray(itemWhiteAndArray[i], itemList) !== -1)
            {
                andedResult = andedResult && true;
            }
            else
            {
                andedResult = andedResult && false;
            }
        }
        //console.log("andedResult: " + andedResult);
        
        if(andedResult)
            inWhiteList = true;
       
    });
    
    inWhiteList = whiteList.length > 0 ? inWhiteList : false;
    
    
    console.log("inWhite: " + inWhiteList + " - inBlack: " + inBlackList);
    
    if((whiteList.length == 0 || inWhiteList) && !inBlackList)
        return true;
    
    return false;
}